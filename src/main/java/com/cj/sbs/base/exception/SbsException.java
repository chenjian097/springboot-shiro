package com.cj.sbs.base.exception;

import com.cj.sbs.base.ReturnCodeEnum;

/**
 * ClassName: SbsException <br/>
 * Function: 自定义异常，用于service <br/>
 * date: 2018/04/26 9:00 <br/>
 *
 * @author chenj
 * @version 1.0.0
 * @since JDK 1.8
 */
public class SbsException extends RuntimeException {

    private ReturnCodeEnum returnCode;

    public SbsException(ReturnCodeEnum returnCode, String message) {
        super(message);
        this.returnCode =returnCode;
    }

    public SbsException(ReturnCodeEnum returnCode) {
        super(returnCode.getMessage());
        this.returnCode =returnCode;
    }

    public ReturnCodeEnum getReturnCode() {
        return returnCode;
    }
}
