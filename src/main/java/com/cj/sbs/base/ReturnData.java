package com.cj.sbs.base;

import lombok.Getter;
import lombok.Setter;

/**
 * ClassName: ReturnData <br/>
 * Function: 页面返回json数据格式 <br/>
 * date: 2018/04/25 13:49 <br/>
 *
 * @author chenj
 * @version 1.0.0
 * @since JDK 1.8
 */
@Getter
@Setter
public class ReturnData<T> {

    private int code;       // 编码
    private String message; // 消息
    private T data;         // 返回数据

    public ReturnData(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public ReturnData(ReturnCodeEnum returnCode, T data) {
        this(returnCode.getCode(), returnCode.getMessage(), data);
    }

    public static ReturnData success() {
        return new ReturnData(ReturnCodeEnum.SUCCESS, null);
    }

    public static <T> ReturnData<T> success(final T data) {
        return new ReturnData<>(ReturnCodeEnum.SUCCESS, data);
    }

    public static ReturnData fail(final ReturnCodeEnum returnCode) {
        return new ReturnData(returnCode, null);
    }

    public static <T> ReturnData<T> fail(final ReturnCodeEnum returnCode, T data) {
        return new ReturnData(returnCode, data);
    }
}
