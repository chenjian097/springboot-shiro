package com.cj.sbs.base;

public enum ReturnCodeEnum {

    SUCCESS(0, "成功"),

    USER_NOT_LOGIN(1000, "用户未登录"),
    USER_ACOUNT_ERROR(1001, "用户名或密码错误"),
    USER_NO_AUTH(1002, "没有权限"),

    PARAM_ERROR(2000, "参数错误"),

    INTERNAL_ERROR(5000, "内部错误");

    private int code;
    private String message;

    ReturnCodeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
