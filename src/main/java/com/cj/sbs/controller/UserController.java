package com.cj.sbs.controller;

import com.cj.sbs.base.ReturnCodeEnum;
import com.cj.sbs.base.ReturnData;
import com.cj.sbs.data.TokenData;
import com.cj.sbs.param.LoginParam;
import com.cj.sbs.service.UserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * ClassName: UserController <br/>
 * Function: user controller <br/>
 * date: 2018/04/24 17:25 <br/>
 *
 * @author chenj
 * @version 1.0.0
 * @since JDK 1.8
 */
@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Resource
    private UserService userService;

    /**
     * 登录
     *
     * @param loginParam
     * @return
     */
    @PostMapping(value = "/login")
    public ReturnData<TokenData> login(@Valid LoginParam loginParam) {
        return ReturnData.success(userService.login(loginParam));
    }

    /**
     * 用户未登录返回数据
     *
     * @return
     */
    @GetMapping(value = "/unauth")
    public ReturnData<Void> unauth() {
        return ReturnData.fail(ReturnCodeEnum.USER_NO_AUTH);
    }

    @GetMapping(value = "/testRoles")
    @RequiresRoles("admin")
    public ReturnData<Void> testRoles() {
        return ReturnData.success();
    }

    @GetMapping(value = "/testPermissions")
    @RequiresPermissions("user:delete")
    public ReturnData<Void> testPermissions() {
        return ReturnData.success();
    }
}
