package com.cj.sbs.data;

import lombok.Getter;
import lombok.Setter;

/**
 * ClassName: TokenData <br/>
 * Function: token data <br/>
 * date: 2018/04/25 14:47 <br/>
 *
 * @author chenj
 * @version 1.0.0
 * @since JDK 1.8
 */
@Getter
@Setter
public class TokenData {

    private String token;

    public TokenData(String token) {
        this.token = token;
    }
}
