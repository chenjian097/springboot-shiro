package com.cj.sbs.service;

import com.cj.sbs.data.TokenData;
import com.cj.sbs.param.LoginParam;
import com.cj.sbs.po.User;

import java.util.List;

public interface UserService {

    User selectByUserName(String userName);

    List<String> queryRolesByUserName(String userName);

    List<String> queryPermissionsByUserName(String userName);

    TokenData login(LoginParam loginParam);
}
