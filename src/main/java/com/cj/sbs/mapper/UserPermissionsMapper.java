package com.cj.sbs.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserPermissionsMapper {

    List<String> queryPermissionsByUserName(String userName);
}