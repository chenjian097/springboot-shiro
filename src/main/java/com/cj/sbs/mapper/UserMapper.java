package com.cj.sbs.mapper;

import com.cj.sbs.po.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper {

    User selectByUserName(String userName);
}