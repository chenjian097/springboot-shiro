package com.cj.sbs.config.shiro.cache;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * ClassName: RedisCache <br/>
 * Function: redis cache <br/>
 * date: 2018/04/24 20:15 <br/>
 *
 * @author chenj
 * @version 1.0.0
 * @since JDK 1.8
 */
@Component
public class RedisCache<K, V> implements Cache<K, V> {

    @Resource
    private RedisTemplate<K, V> redisTemplate;

    @Value("${sbs.shiro.cache.prefix}")
    private String SBS_CACHE_PREFIX;
    @Value("${sbs.shiro.cache.expire}")
    private int expire;

    private K getCacheKey(Object k) {
        return (K) (SBS_CACHE_PREFIX + k);
    }

    @Override
    public V get(K k) throws CacheException {
        System.out.println("从redis中获取");
        return redisTemplate.opsForValue().get(getCacheKey(k));
    }

    @Override
    public V put(K k, V v) throws CacheException {
        System.out.println("放入redis缓存");
        K cacheKey = getCacheKey(k);
        redisTemplate.boundValueOps(cacheKey).set(v);
        redisTemplate.opsForValue().set(cacheKey, v, expire, TimeUnit.SECONDS);
        return v;
    }

    @Override
    public V remove(K k) throws CacheException {
        V old = get(k);
        redisTemplate.delete(getCacheKey(k));
        return old;
    }

    @Override
    public void clear() throws CacheException {
        redisTemplate.delete(keys());
    }

    @Override
    public int size() {
        return keys().size();
    }

    @Override
    public Set<K> keys() {
        return redisTemplate.keys(getCacheKey("*"));
    }

    @Override
    public Collection<V> values() {
        Set<K> set = keys();
        List<V> list = new ArrayList<>();
        for (K s : set) {
            list.add(get(s));
        }
        return list;
    }
}
