package com.cj.sbs.config.shiro;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * ClassName: JWTToken <br/>
 * Function: jwt token <br/>
 * date: 2018/04/25 15:22 <br/>
 *
 * @author chenj
 * @version 1.0.0
 * @since JDK 1.8
 */
public class JWTToken implements AuthenticationToken {

    // 密钥
    private String token;

    public JWTToken(String token) {
        this.token = token;
    }

    @Override
    public Object getPrincipal() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }
}
