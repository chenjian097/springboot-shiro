package com.cj.sbs.po;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRoles {
    private Integer id;

    private String username;

    private String roleName;

}