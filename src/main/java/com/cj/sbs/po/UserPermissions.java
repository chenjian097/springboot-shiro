package com.cj.sbs.po;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserPermissions {

    private Integer id;

    private String userPermission;

    private String username;

}