# springboot-shiro

#### 项目介绍
spring boot整合shiro、jwt、redis的简单例子

#### 使用技术
1. JWT（java web token）
1. 用户、权限认证：shiro
1. 缓存：redis
1. 骨架：spring-boot
1. 数据库：mysql

#### 待优化
1. JWT token失效更新
1. shiro filter异常处理，现在不能直接返回错误，只能通过跳转的方式